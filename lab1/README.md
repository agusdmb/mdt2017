Minería de Datos para Texto 2017
================================

Práctico de Clustering
----------------------

#### Alumno: Agustín Márquez
- email: agusdmb@gmail.com

#### Selección de Corpus

Se utilizó como corpus para el laboratorio el archivo de texto del diario la voz
que se encuentra en el directorio de la profesora Laura Alonso. El mismo se
eligió porque además de ser un corpus de la lengua española, esta hecho en base
a un diario local.

Lo podemos obtener con el siguiente comando:


```
wget https://cs.famaf.unc.edu.ar/~laura/corpus/lavoztextodump.txt.tar.gz && \
tar -xf lavoztextodump.txt.tar.gz
```


#### Preprocesamiento del corpus

Para el preprocesamiento se probó con diferentes librerias, como *nltk* (para la
tokenización) y *spacy* (para el Part Of Speech Tagging). Las mismas se pueden
conseguir de los siguientes links:

- http://www.nltk.org/
- https://spacy.io/

Primero se utilizó nltk, con la cual se hizo más fácil la tokenización del
corpus. Pero se dificultaba más para el Part Of Speech Tagging (a partir de
ahora pos tag). Por lo que aquí entró en juego spacy, la cual es una librería
muy facil de usar, aunque mas lenta, ya que apenas inicializada con el corpus,
hace muchos análisis del mismo por defecto lo cual lleva algo más de tiempo,
entre otros, nos hace la tokenización y el pos tag.

A continación dejaré la parte del código de preprocesamiento más relevante,
usando nltk.

```python
import nltk

sents = nltk.sent_tokenize(raw_text)

sents_tokenized = []
for sent in sents:
    sents_tokenized.append(nltk.word_tokenize(sent))

sents_tokenized_filtered = []
for sent in sents_tokenized:
    sents_tokenized_filtered.append([
        word.lower() for word in sent if word.isalpha() and len(word) > 3
    ])
```

Como podemos ver en el código, se dicidió filtrar los tokenicers para que sólo
queden las palabras (no números, ni símbolos) que tengan más de 3 letras. Esto
se hizo para lograr mayor velocidad de preprocesamiento y posteriormente de
análisis, descartando lo que yo consideré como las palabras menos relevantes.

Para procesar el corpus con spacy, es tan fácil como:

```python
import spacy

nlp = spacy.load('es')
doc = nlp(raw_text)
```

Una vez que termina de ejecutar la última línea, el 'doc' creado contiene ya el
corpus tokenizado por sentencias, el pos tag, y muchos otros features más.

#### Creación de matrices para posterior clusterización

Esta sin duda fue la parte más difícil del proyecto, ya que me encontré con
muchos problemas a la hora de correr el código que iba haciendo. Constatemente
tuve que modificarlo para hacer los algoritmos mas eficientes, tanto en
velocidad como en uso de memoria. Entre los cambios más significativos estuvo,
el uso *dok_matrix*, ya que la matríz generada en diferentes ocaciones tenía
valores muy dispersos, por lo que la gran mayoría eran sólo ceros. Este fue el
cambio que mayor mejora tuvo en cuanto a la utilización de memoria. Para mejorar
la velocidad, en distintas partes del código se reimplementaron funciones usando
*bisect*, una librería que mantiene ordenado un arreglo y usa el algoritmo de
bisección para buscar o insertar elementos.

```python
from scipy.sparse import dok_matrix
import bisect

def get_vocab(doc):
    def add_vocab(vocab, token):
        position = bisect.bisect_left(vocab, token)
        if position == len(vocab) or vocab[position] != token:
            vocab.insert(position, token)
        return vocab

    vocab = []
    for sent in doc:
        for token in sent:
            vocab = add_vocab(vocab, token)

    return vocab


def make_matrix(vocab, doc):
    def find_position(vocab, w):
        position = bisect.bisect_left(vocab, w)
        return position

    matrix = dok_matrix((len(vocab), len(vocab)))

    for sent in doc:
        for i in range(len(sent) - 1):
            w1 = sent[i]
            w2 = sent[i + 1]
            p1 = find_position(vocab, w1)
            p2 = find_position(vocab, w2)
            matrix[p1, p2] += 1

    return matrix
```

En estos dos ejemplos podemos ver como se usó *bisect* en *get_vocab*, una
función que a partir de la lista de sentencias tokenizadas devuelve el
vocabulario de las mismas (o sea, una lista con todas las palabras distintas que
aparecen en las mismas, ordenada). Y en *make_matrix* podemos ver además del uso
de *bisect* nuevamente, el uso de *dok_matrix*. Originalmente esta función hacía
uso de *ngrams* de *nltk*, y luego se sumaba la cantidad de ngramas iguales con
la función *Counter* de las *collections* pero con la nueva implementación se
aumento la velocidad considerablemente.

Se crearon diversas matrices con distintos features, usando ngramas y pos tags.
Probando que opciones eran viables, considerando los 4GB de RAM que posee la
computadora utilizada y su limitada capacidad de cálculo.

#### Clustering

Para el clustering se utilizó la implementación *KMeans* de la librería
*scikit-learning*. Esta parte fue la de menor implementación y mayor prueba y
error, ya que usar esta librería resultó ser muy sencillo.

- http://scikit-learn.org/stable/

Tan sencillo como tan sólo importarla, crear una instancia nueva de KMeans, la
cual al inicializarla se le pasan ciertos parámetros y luego pasarle la matríz.
Los parámetros que probé variar buscando los mejores resultados fueron: Cantidad
de clusters (por defecto 8) aunque normalmente se probó con menos cantidad, como
3, 4 o 5 clusters. Máximio numero de iteraciones para armar los clusters, por
defecto son 300, y se probo con valores entre 100 y 500. Numero de jobs, se
seteo siempre a '-1' ya que de este modo utiliza todos los cores del sistema. Y
el algoritmo a usar, que se uso siempre en 'full' ya que el otro algoritmo,
'elkan' falla cuando la matríz tiene datos dispersos, lo cual era este el caso.

El código es muy sencillo:

```python
from sklearn.cluster import KMeans


km = KMeans(
    n_clusters=5,
    max_iter=300,
    n_jobs=-1,
    algorithm='full')
km.fit(matrix)
```

Y Para poder analizar de manera mas visual la salida del KMeans, se implementó
la siguiente clase para imprimir los clusters:

```python
import pickle


class clusters():
    def __init__(self, groups, words, n):
        self.groups = groups
        self.words = words
        self.n = n

    def print_group(self, n):
        print("Cluster: " + str(n))
        i = 0
        for elem in self.groups:
            if elem == n:
                print(self.words[i])
            i += 1
        print("Total words in cluster " + str(n) + ": " +
              str(Counter(self.groups)[n]))

    def print_groups(self):
        for i in range(self.n):
            self.print_group(i)

    def save(self, file):
        with open(file, 'wb') as f:
            pickle.dump(self, f)
```

#### Resultados

Los resultados obtenidos en los clusters no fueron los esperados, ya que
normalmente se encontraban 2 o 3 clusters como máximo con muchos elementos y el
resto eran singletons (o sea clases con un único elemento, o muy poquitos). A
medida que se incrementaba el número de cantidad de clusters, más singletons se
econtraban. A pesar de los intentos por lograr una mejor agrupación en ellos me
econtré muchas veces en situaciones donde la RAM no alcanzaba y la swap clavaba,
resultando en *Memory Error*. Otras tantas en tener que esperar hasta 2 horas en
que parte del texto se procese.

#### Conclusión

Dado los resultados, se puede concluir que las tareas de procesar texto y
clustering requieren de conocimientos avanzados y buenas habilidades de
programación. Ya que para poder procesar todo de una manera inteligente y
eficiente, cuando se trata de grandes volúmenes de datos, la prueba y
error no es una opción por la cantidad de tiempo que lleva cada ejecución del
programa.
