from collections import Counter
from nltk.util import ngrams
from scipy.sparse import dok_matrix
from sklearn.cluster import KMeans
import bisect
import json
import nltk
import pickle
import spacy

SOURCE_FILE = './lavoztextodump.txt'
TOKENIZED_FILE = './text_tokenized.json'
NGRAM_FILE = './ngram'
N_GRAM = 2


def load_file(filename):
    print("Loading")
    with open(filename, 'r') as f:
        data = f.read()

    print("Done")
    return data


def save_json(filename, data):
    with open(filename, 'w') as f:
        json.dump(data, f)


def load_json(filename):
    with open(filename, 'r') as f:
        data = json.load(f)

    return data


def save_pickle(filename, data):
    with open(filename, 'wb') as f:
        pickle.dump(data, f)


def load_pickle(filename):
    with open(filename, 'rb') as f:
        data = pickle.load(f)

    return data


def raw_to_tokenize_sents(raw_text):
    print("Sent tokenizing")
    sents = nltk.sent_tokenize(raw_text)

    sents_tokenized = []

    print("Word tokenizing")
    # total = len(sents)
    # done = 0
    # for sent in sents[:10000]:
    for sent in sents:
        sents_tokenized.append(nltk.word_tokenize(sent))
        # done += 1
        # print(total, done)
    # input()

    sents_tokenized_filtered = []

    # print(len(sents_tokenized))
    print("Filtering")
    # total = len(sents_tokenized)
    # done = 0
    for sent in sents_tokenized:
        sents_tokenized_filtered.append([
            word.lower() for word in sent if word.isalpha() and len(word) > 3
        ])
        # done += 1
        # print(total, done)

    # input()

    print("Done")
    return sents_tokenized_filtered


def get_vocab(doc):
    def add_vocab(vocab, token):
        position = bisect.bisect_left(vocab, token)
        if position == len(vocab) or vocab[position] != token:
            vocab.insert(position, token)

        return vocab

    print("Making vocab")

    vocab = []
    # total = len(doc)
    # done = 0
    for sent in doc:
        for token in sent:
            vocab = add_vocab(vocab, token)
        # done += 1
        # print(total, done)

    print("Done")
    return vocab


def make_ngrams(data, n=2):
    print("Making ngram")
    frequencies = Counter([])
    total = len(data)
    done = 0
    for sent in data:
        ngram = ngrams(sent, n)
        frequencies += Counter(ngram)
        done += 1
        print(total, done)
    # input()

    return frequencies


def make_matrix(vocab, doc):
    def find_position(vocab, w):
        position = bisect.bisect_left(vocab, w)
        return position

    print("Making matrix")
    matrix = dok_matrix((len(vocab), len(vocab)))

    # total = len(doc)
    # done = 0
    for sent in doc:
        for i in range(len(sent) - 1):
            w1 = sent[i]
            w2 = sent[i + 1]
            p1 = find_position(vocab, w1)
            p2 = find_position(vocab, w2)
            matrix[p1, p2] += 1
            # if matrix[p1, p2] > 1:
            #     print(w1, w2)
            #     input()
        # done += 1
        # print(total, done)

    print("Done")
    return matrix


def parse_file():
    # This took 1:14 on my computer
    print("Loading")
    raw_text = load_file(SOURCE_FILE)
    print("Tokenizing")
    sents_tokenized = raw_to_tokenize_sents(raw_text)
    print("Saving")
    save_json(TOKENIZED_FILE, sents_tokenized)


def ngrams_file():
    # This took 112 minutes
    print("Loading tokenized_file")
    sents_tokenized = load_json(TOKENIZED_FILE)
    print("Making ngram")
    ngram = make_ngrams(sents_tokenized, N_GRAM)
    print("Saving")
    save_pickle(NGRAM_FILE, ngram)


def spacy_file():
    print("Loading spacy")
    nlp = spacy.load('es')
    print("Loading")
    raw_text = load_file(SOURCE_FILE)
    doc = nlp(raw_text)

    return doc


def make_clusters(matrix, c):
    km = KMeans(n_clusters=5, max_iter=300, n_jobs=-1, algorithm='full')
    km.fit(matrix)
    return km


class clusters():
    def __init__(self, groups, words, n):
        self.groups = groups
        self.words = words
        self.n = n

    def print_group(self, n):
        print("Cluster: " + str(n))
        i = 0
        for elem in self.groups:
            if elem == n:
                print(self.words[i])
            i += 1
        print("Total words in cluster " + str(n) + ": " +
              str(Counter(self.groups)[n]))

    def print_groups(self):
        for i in range(self.n):
            self.print_group(i)

    def save(self, file):
        with open(file, 'wb') as f:
            pickle.dump(self, f)


raw_text = load_file(SOURCE_FILE)
doc = raw_to_tokenize_sents(raw_text)
vocab = get_vocab(doc)
matrix = make_matrix(vocab, doc)
