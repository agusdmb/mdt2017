from collections import Counter
from nltk.corpus import stopwords
from sklearn.cluster import KMeans
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import chi2
import bisect
import codecs
import pickle

CORPUS = 'spanishEtiquetado_10000_15000'
WORD = 0
LEMMA = 1
POS = 2
SENSE = 3


class Tokenizer():
    def __init__(self, doc):
        print("Tokenizing doc")
        self.tokens = []
        for line in doc:
            token = self._tokenize_line(line)
            if token is not None:
                self.tokens.append(token)
        print("Done")

    def _tokenize_line(self, line):
        if len(line) > 0 and line[0] != '<':
            try:
                (word, lemma, pos, sense) = line.split(' ')
                if pos[0] != 'F' and lemma.isalpha() and \
                        lemma not in stopwords.words('spanish'):
                    return (word.lower(), lemma, pos, sense)
            except ValueError:
                return None
        return None

    def get_tokens(self):
        return self.tokens


class Vocab():
    def __init__(self, tokenizer):
        print("Making vocab")
        tokens = tokenizer.get_tokens()
        self.vocab = []
        self.pos = []
        for token in tokens:
            self._add_vocab(token[WORD], token[POS])
        print("Done")

    def _add_vocab(self, word, pos):
        position = bisect.bisect_left(self.vocab, word)
        if position == len(self.vocab) or self.vocab[position] != word:
            self.vocab.insert(position, word)
            self.pos.insert(position, pos)

    def get_vocab(self):
        return self.vocab

    def get_pos(self):
        return self.pos


class Features():
    def __init__(self, vocab):
        self.vocab = vocab.get_vocab()
        self.features = [{} for _ in self.vocab]
        self.normalized = False

    def add_feature(self, word, feature):
        if not self.normalized:
            position = bisect.bisect_left(self.vocab, word)
            if feature in self.features[position]:
                self.features[position][feature] += 1
            else:
                self.features[position][feature] = 1
        else:
            raise Exception("Can't add new features. X already normalized")

    def get_features(self):
        return self.features

    def normalize(self):
        if not self.normalized:
            print("Normalizing")
            for feature in self.features:
                maximum = max(feature.values())
                for key in feature.keys():
                    feature[key] = feature[key] / maximum
            self.normalized = True
            print("Done")


class Clusters():
    def __init__(self, groups, words, n):
        self.groups = groups
        self.words = words
        self.n = n

    def print_group(self, n):
        print("Cluster: " + str(n))
        i = 0
        for elem in self.groups:
            if elem == n:
                print(self.words[i])
            i += 1
        print("Total words in cluster " + str(n) + ": " +
              str(Counter(self.groups)[n]))

    def print_groups(self):
        for i in range(self.n):
            self.print_group(i)
            input()  # wait for an enter before continue printing

    def print_count_groups(self):
        print(Counter(self.groups))

    def save(self, file):
        with open(file, 'wb') as f:
            pickle.dump(self, f)


class Nlp():
    def __init__(self):
        self.doc = self.load_file()
        self.tokenizer = Tokenizer(self.doc[:100000])
        self.vocab = Vocab(self.tokenizer)
        self.features = self.get_features()
        self.features.normalize()
        self.vectorizar()
        self.varianze_remove(.9 * (1 - .9))
        self.univariate_features()

    def load_file(self):
        print("Loading")
        with codecs.open(CORPUS, 'r', 'iso-8859-1') as f:
            doc = f.readlines()
        print("Done")
        return doc

    def get_features(self):
        print("Getting features")
        tokens = self.tokenizer.get_tokens()
        features = Features(self.vocab)
        for i in range(len(tokens)):
            token = tokens[i]
            # features.add_feature(token[0], 'actual_pos=' + token[2])
            if i > 0:
                previous_token = tokens[i - 1]
                features.add_feature(token[WORD],
                                     'previous_token=' + previous_token[LEMMA])
                features.add_feature(token[WORD],
                                     'previous_pos=' + previous_token[POS])
            if i < len(tokens) - 1:
                next_token = tokens[i + 1]
                features.add_feature(token[WORD],
                                     'next_token=' + next_token[LEMMA])
                features.add_feature(token[WORD],
                                     'next_pos=' + next_token[POS])

        print("Done")
        return features

    def vectorizar(self):
        print("Vectorizando")
        v = DictVectorizer()
        self.x = v.fit_transform(self.features.get_features())
        print("Done")

    def varianze_remove(self, var):
        print("Seleccionando por varianza")
        sel = VarianceThreshold(threshold=(var))
        self.var_x = sel.fit_transform(self.x)
        print("Done")

    def univariate_features(self):
        print("Univariate")
        self.pos_x = SelectKBest(
            chi2, k=1000).fit_transform(self.x, self.vocab.get_pos())
        print("Done")

    def clusterizar(self, x, n_clusters):
        km = KMeans(n_clusters=n_clusters, n_jobs=-1).fit(x)
        clusters = Clusters(km.labels_, self.vocab.get_vocab(), n_clusters)
        return clusters
