Minería de Datos para Texto 2017
================================

Práctico de Feature Selection
----------------------

#### Alumno: Agustín Márquez
- email: agusdmb@gmail.com

#### Corpus

Se utilizó como corpus para el laboratorio el Wikicorpus provisto en las
consignas del mismo. El mismo tenía las ventajas de estar marcado con lemma,
POS y Sense. Este corpus se uso anotado y no anotado para los distintos puntos
del laboratorio.

#### Librerías utilizadas

Se utilizaron las librerías de *nltk*, *SciKit Learning*. De la primera se
utilizaron los stopwords, y de la segunda en especial para las tareas de este
laboratorio en particular, se usó de *feature_selection* *SelectKBest* y
*VarianceThreshold*

#### Feature selection supervisado

A diferencia del laboratorio anterior, al momento de tokenizar el corpus, como
el mismo estaba anotado, se tokenizaron la palabras ya con sus respectivos
lemmas, pos y sense, todo a la vez. Luego se generaron dos listas, una con cada
palabra única, y otra con el pos de dicha palabra. Las listas están ordenadas
por palabra, para hacer la busqueda mas eficiente. Se utilizó el mismo proceso
que en el laboratorio anterior para la creación de un diccionario de features
por palabra. Luego este diccionario se pasó a una matriz con la función
DictVectorizer. Después se seleccionaron de la matriz los *K* features más
relevantes. Las pruebas para este laboratorio se hicieron con valores para *K*
de 100 a 1000. Para hacer esta selección se usó *SelectKBest*. Esta clase
recive como parámetro, además de la cantidad *K* de features a seleccionar, una
función con la cual calcular el puntaje de cada feature para así elegir cuales
son los *K* mejores y la lista de pos que se hizo al momento de tokenizar. La
función que se usó para esto fue *chi2* también de *SciKit Learning*.
Finalmente la matriz obtenida a partir de esta clase es la que se uso para
clusterizar, nuevamente como en el laboratorio anterior, utilizando *KMeans*
pidiendole esta vez una gran cantidad de clusters, entre 50 y llegando a veces
hasta 500 clusters según que porcentaje del corpus se usaba.

#### Feature selection no supervisado

La diferencia con el método de selección supervisado con no supervisado fue que
en este caso no se uso la lista de pos como targets para seleccionar los
mejores features. En cambio lo que se hizo fue sobre la matriz de features
quitar todos los que tenian una varianza menor a cierto valor. El por qué de
esto es que los features que varían poco, aportan poca información. Por ejemplo
si tenemos un feature que tiene el mismo valor para todas las palabras, la
varianza del mismo es 0, y por ende esto no nos aporta nada de información. Se
probó con distintos valores para descartar estos features, tratando de quitar
los que menos información nos dan pero intentando a la vez no quitar de más.
Para clusterizar se utilizó el mismo método ya antes mencionado.

#### Resultados

Los resultados obtenidos en los clusters fueron notablemente mejores que los
del laboratorio anterior. Normalmente una clase tenia notablemente mas palabras
que los otros, pero en los otros la distribución de las palabras era casi igual
en todos encontrando rara vez clases con pocos elementos. Además hubo una gran
mejora en la cantidad de memoria usada por el programa, así como también
mejoras en la velocidad de ejecución del mismo. Y en comparación entre los dos
tipos de selecciones se encontró sin dudas que el supervisado es agrupa mejor
las palabras que el no supervisado, sin lugar a dudas gracias a que posee mayor
información para elegir los mejores features.
