DATASET_PATH = '20news-18828/'
DATASET_LYRICS_PATH = 'raw_lyrics/'

NEWS_DOC_FILENAME = '20news_docs'
SONGS_DOC_FILENAME = 'songs_docs'

SENTS_NEWS = 'sents'
SENTS_SONGS = 'sents_song'

CATEGORIES_FILENAME = 'categories'

NEWS_EMB_MODEL = '20new-d100-m5-w5'
SONGS_EMB_MODEL = 'songs-d100-m5-w5'
NEWS_SONGS_EMB_MODEL = '20new_songs-d100-m5-w5'

NEWS_EMB_WEIGHT = 'weight_20news'
SONGS_EMB_WEIGHT = 'weight_song'
NEWS_SONGS_EMB_WEIGHT = 'weight_20news_song'

DIMENSION = 100

MODEL_NORMAL_FILENAME = 'model/model_normal'
MODEL_EMB_FILENAME = 'model/model_emb_normal'
MODEL_EMB_NEW_FILENAME = 'model/model_emb_new_normal'
MODEL_EMB_SONG_FILENAME = 'model/model_emb_song_normal'
MODEL_EMB_NEW_SONG_FILENAME = 'model/model_emb_new_song_normal'

EMBEDDINGS_FILENAME = 'glove.6B.100d.txt'

TEST_SIZE = 0.1
VAL_SIZE = 0.1
MAX_WORDS = 5000
BATCH_SIZE = 256
#EPOCHS = 10
PAD = 100